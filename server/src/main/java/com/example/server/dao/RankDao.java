package com.example.server.dao;

import com.example.server.entity.Rank;
import org.springframework.stereotype.Repository;

@Repository
public interface RankDao {

    int insert(Rank record);

    int insertSelective(Rank record);

    //查歌单的总分
    int selectScoreSum(Long songListId);

    //查歌单的总评分人数
    int selectRankNum(Long songListId);
}
