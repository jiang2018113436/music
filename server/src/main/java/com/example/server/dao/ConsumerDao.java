package com.example.server.dao;

import com.example.server.entity.Consumer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsumerDao {

    int deleteByPrimaryKey(Integer id);

    int insert(Consumer consumer);

    int insertSelective(Consumer consumer);

    Consumer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Consumer consumer);

    int updateByPrimaryKey(Consumer consumer);

    int verifyPassword(@Param("username") String username,@Param("password") String password);

    int existUsername(String username);

    int addUser(Consumer consumer);

    int updateUserMsg(Consumer record);

    int updateUserAvator(Consumer record);

    int deleteUser(Integer id);

    List<Consumer> allUser();

    List<Consumer> userOfId(Integer id);

    List<Consumer> loginStatus(String username);

}
