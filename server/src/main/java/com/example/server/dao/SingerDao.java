package com.example.server.dao;


import com.example.server.entity.Singer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SingerDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Singer singer);

    int insertSelective(Singer singer);

    Singer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Singer singer);

    int updateByPrimaryKey(Singer singer);

    int updateSingerMsg(Singer singer);

    int updateSingerPic(Singer singer);

    int deleteSinger(Integer id);

    List<Singer> allSinger();

    List<Singer> singerOfName(String name);

    List<Singer> singerOfSex(Integer sex);
}
