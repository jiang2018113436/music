package com.example.server.dao;


import com.example.server.entity.ListSong;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListSongDao {
    // 插入数据
    int insertSelective(ListSong listSong);
    // 查询
    ListSong selectByPrimaryKey(Integer id);
    // 全修改
    int updateByPrimaryKey(ListSong listSong);
    // 选择性修改
    int updateListSongMsg(ListSong listSong);
    // 判断歌单里是否存在歌
    int existListSong(@Param("songId") Integer songId,@Param("songListId") Integer songListId);
    // 删除歌单里某首歌
    int deleteListSongOfSong(@Param("songId") Integer songId,@Param("songListId") Integer songListId);
    // 删除歌单和歌单里所有歌
    int deleteListSong(Integer songListId);
    // 查询所有
    List<ListSong> allListSong();
    // 查询歌单所含的所有音乐
    List<ListSong> listSongOfSongId(Integer songListId);
}
