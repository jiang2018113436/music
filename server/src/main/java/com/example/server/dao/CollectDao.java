package com.example.server.dao;

import com.example.server.entity.Collect;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectDao {
    int deleteByPrimaryKey(Integer id);
    //插入
    int insert(Collect collect);
    //选择性插入数据
    int insertSelective(Collect collect);
    //查询
    Collect selectByPrimaryKey(Integer id);
    //根据ID更新
    int updateByPrimaryKey(Collect collect);
    //判断歌
    int existSongId(@Param("userId") Integer userId, @Param("songId") Integer songId);
    //更新收藏夹 选择性更新
    int updateCollectMsg(Collect collect);
    //删除收藏歌单
    int deleteCollect(@Param("userId") Integer userId, @Param("songId") Integer songId);
    //查询全部歌单
    List<Collect> allCollect();
    //用户歌单
    List<Collect> collectionOfUser(Integer userId);
}
