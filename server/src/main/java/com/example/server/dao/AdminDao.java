package com.example.server.dao;

import com.example.server.entity.Admin;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminDao {
    //删除
    int deleteByPrimaryKey(Integer id);
    //插入
    int insert(Admin admin);
    //选择插入某个字段
    int insertSelective(Admin admin);
    //根据ID查询
    Admin selectByPrimaryKey(Integer id);
    //修改某字段
    int updateByPrimaryKeySelective(Admin admin);
    //修改
    int updateByPrimaryKey(Admin admin);
    //验证密码
    int verifyPassword(String username, String password);
}
