package com.example.server.dao;

import com.example.server.entity.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentDao {

    int insert(Comment record);
    int insertSelective(Comment record);
    int updateCommentMsg(Comment record);
    int deleteComment(Integer id);

    List<Comment> allComment();
    List<Comment> commentOfSongId(Integer songId);
    List<Comment> commentOfSongListId(Integer songListId);

    Comment selectByPrimaryKey(Integer id);
    int deleteByPrimaryKey(Integer id);
    int updateByPrimaryKey(Comment record);
    int updateByPrimaryKeySelective(Comment record);
}
