package com.example.server.web;

import com.alibaba.fastjson.JSONObject;
import com.example.server.entity.ListSong;
import com.example.server.service.ListSongService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/listSong")
public class ListSongController {
    @Resource
    private ListSongService service;
    private static final Logger log = LoggerFactory.getLogger(AdminController.class);
    // 返回所有歌单所对应的所有歌
    @GetMapping("/")
    public List<ListSong> getAll(){
        log.info("访问了所有歌单接口");
        return service.allListSong();
    }

    //添加音乐到歌单
    @PostMapping("/add")
    public Object addListSong(ListSong listSong){
        log.info("访问了添加音乐到歌单接口");
        JSONObject jsonObject = new JSONObject();
        Integer songId = listSong.getSongId();
        Integer ListSongId = listSong.getSongListId();
        if(songId == null || "".equals(songId) || ListSongId == null || "".equals(ListSongId)){
            jsonObject.put("code",0);
            jsonObject.put("msg","错误的ID");
            return jsonObject;
        }else if(service.existListSong(songId,ListSongId) > 0){
            jsonObject.put("code",2);
            jsonObject.put("msg","已添加");
            return jsonObject;
        }
        int r = service.insertSelective(listSong);
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","添加成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","添加失败");
            return  jsonObject;
        }
    }

    // 指定歌单ID的歌曲
    @GetMapping("/detail")
    public Object ListSongOfSongID(Integer songListId){
        log.info("访问了指定歌单ID的歌曲接口");
        return service.listSongOfSongId(songListId);

    }

    //    删除指定歌单里的指定歌曲
    @GetMapping("/delete")
    public Object deleteListSong(Integer songId,Integer ListSongId){
        log.info("访问了删除指定歌单里的指定歌曲接口");
        JSONObject jsonObject = new JSONObject();
        System.out.println(songId);
        System.out.println(ListSongId);
        int r = service.deleteListSongOfSong(songId,ListSongId);
        if(songId == null || "".equals(songId) || ListSongId == null || "".equals(ListSongId)){
            jsonObject.put("code",0);
            jsonObject.put("msg","错误的ID");
            return jsonObject;
        }
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","删除成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","删除失败");
            return  jsonObject;
        }
    }


    // 修改歌单或者歌曲信息
    @PostMapping("/update")
    public Object updateListSong(ListSong listSong){
        log.info("访问了修改歌单或者歌曲信息接口");
        JSONObject jsonObject = new JSONObject();

        int r = service.updateListSongMsg(listSong);
        if (r > 0 ){
            jsonObject.put("code",1);
            jsonObject.put("msg","修改成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","修改失败");
            return jsonObject;
        }
    }
}
