package com.example.server.web;

import com.alibaba.fastjson.JSONObject;
import com.example.server.constant.Constants;
import com.example.server.entity.Consumer;
import com.example.server.service.ConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@RestController
@CrossOrigin(allowCredentials = "true",origins = "http://127.0.0.1:8888")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;
    private static final Logger log = LoggerFactory.getLogger(AdminController.class);


    @Configuration
    public class MyPicConfig implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            String os = System.getProperty("os.name");
            if (os.toLowerCase().startsWith("win")) {
                registry.addResourceHandler("/img/avatorImages/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_WIN_PATH + "\\img\\avatorImages\\");
            } else {
                registry.addResourceHandler("/img/avatorImages/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_MAC_PATH + "/img/avatorImages/");
            }
        }
    }

//    添加用户
    @PostMapping("/user/add")
    public Object addUser(Consumer consumer){
        log.info("访问了添加用户接口");
        JSONObject jsonObject = new JSONObject();
        consumer.setCreateTime(new Date());
        consumer.setUpdateTime(new Date());
        if (consumer.getUsername().equals("") || consumer.getUsername() == null){
            jsonObject.put("code", 0);
            jsonObject.put("msg", "用户名或密码错误");
            return jsonObject;
        }
        int r = consumerService.veritypasswd(consumer.getUsername(), consumer.getPassword());
        if (r>0){
            jsonObject.put("msg", "该用户名已存在，请更该用户名！");
            return jsonObject;
        }else {
            if (consumer.getPhoneNum() == "") {
                consumer.setPhoneNum(null);
            } else {
                consumer.setPhoneNum(consumer.getPhoneNum());
            }
            if (consumer.getEmail() == "") {
                consumer.setEmail(null);
            } else {
                consumer.setEmail(consumer.getEmail());
            }
            int res = consumerService.addUser(consumer);
            if (res > 0) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "注册成功");
                return jsonObject;
            } else {
                jsonObject.put("code", 0);
                jsonObject.put("msg", "注册失败");
                return jsonObject;
            }
        }
    }

//    判断是否登录        成功
    @PostMapping("/user/login/status")
    public Object loginStatus(Consumer consumer,HttpSession session){
        log.info("访问了登录接口");
        JSONObject jsonObject = new JSONObject();
        String username = consumer.getUsername();
        String password = consumer.getPassword();
        int res = consumerService.veritypasswd(username, password);
        if (res>0){
            jsonObject.put("code", 1);
            jsonObject.put("msg", "登录成功");
            jsonObject.put("userMsg", consumerService.loginStatus(username));
            session.setAttribute("username", username);
            return jsonObject;
        }else {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "用户名或密码错误");
            return jsonObject;
        }

    }

//    返回所有用户        成功
    @GetMapping ("/user")
    public Object allUser(){
        log.info("访问了获取所有用户接口");
        return consumerService.allUser();
    }

//    返回指定ID的用户     成功
    @GetMapping( "/user/detail")
    public Object userOfId(HttpServletRequest req){
        log.info("访问了获取指定ID的用户接口");
        String id = req.getParameter("id");
        return consumerService.userOfId(Integer.parseInt(id));
    }

//    删除用户       成功
    @GetMapping("/user/delete")
    public Object deleteUser(HttpServletRequest req){
        String id = req.getParameter("id");
        return consumerService.deleteUser(Integer.parseInt(id));
    }

//    更新用户信息    成功
    @PostMapping( "/user/update")
    public Object updateUserMsg(Consumer consumer){
        JSONObject jsonObject = new JSONObject();
        Integer id = consumer.getId();
        String username = consumer.getUsername();
        String password = consumer.getPassword();
        Byte sex = consumer.getSex();
        String phoneNum = consumer.getPhoneNum();
        String email = consumer.getEmail();
        String birth = consumer.getBirth();
        String introduction = consumer.getIntroduction();
        String location = consumer.getLocation();

        if (username.equals("") || username == null){
            jsonObject.put("code", 0);
            jsonObject.put("msg", "用户名或密码错误");
            return jsonObject;
        }
        Consumer user = new Consumer();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
        user.setSex(new Byte(sex));
        user.setPhoneNum(phoneNum);
        user.setEmail(email);
        user.setBirth(String.valueOf(birth));
        user.setIntroduction(introduction);
        user.setLocation(location);
        user.setUpdateTime(new Date());

        int res = consumerService.updateUserMsg(user);
        if (res>0){
            jsonObject.put("code", 1);
            jsonObject.put("msg", "修改成功");
            return jsonObject;
        }else {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "修改失败");
            return jsonObject;
        }
    }

//    更新用户头像
    @PostMapping("/user/avatar/update")
    public Object updateUserPic(@RequestParam("file") MultipartFile avatorFile, @RequestParam("id")int id){
        JSONObject jsonObject = new JSONObject();

        if (avatorFile.isEmpty()) {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "文件上传失败！");
            return jsonObject;
        }
        String fileName = System.currentTimeMillis()+avatorFile.getOriginalFilename();
        String filePath = System.getProperty("user.dir") + System.getProperty("file.separator")+"server\\"  + "img" + System.getProperty("file.separator") + "avatorImages" ;
        File file1 = new File(filePath);
        if (!file1.exists()){
            file1.mkdir();
        }

        File dest = new File(filePath + System.getProperty("file.separator") + fileName);
        String storeAvatorPath = "/img/avatorImages/"+fileName;
        try {
            avatorFile.transferTo(dest);
            Consumer consumer = new Consumer();
            consumer.setId(id);
            consumer.setAvator(storeAvatorPath);
            int res = consumerService.updateUserAvator(consumer);
            if (res>0){
                jsonObject.put("code", 1);
                jsonObject.put("avator", storeAvatorPath);
                jsonObject.put("msg", "上传成功");
                return jsonObject;
            }else {
                jsonObject.put("code", 0);
                jsonObject.put("msg", "上传失败");
                return jsonObject;
            }
        }catch (IOException e){
            jsonObject.put("code", 0);
            jsonObject.put("msg", "上传失败"+e.getMessage());
            return jsonObject;
        }finally {
            return jsonObject;
        }
    }
}
