package com.example.server.web;


import com.alibaba.fastjson.JSONObject;
import com.example.server.constant.Constants;
import com.example.server.entity.SongList;
import com.example.server.service.SongListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.io.*;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/songList")
public class SongListController {
    @Resource
    private SongListService service;
    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    // 配置过滤器
    @Configuration
    public class MyPicConfig implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            String os = System.getProperty("os.name");
            if (os.toLowerCase().startsWith("win")) { // windos系统
                registry.addResourceHandler("/img/songListPic/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_WIN_PATH + "\\img\\songListPic\\");
            } else { // MAC、Linux系统
                registry.addResourceHandler("/img/songListPic/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_MAC_PATH + "/img/songListPic/");
            }
        }
    }

    // 所有歌单
    @GetMapping("")
    public List<SongList> getAll(){
        log.info("访问了所有歌单接口");
        return service.allSongList();
    }

    // 模糊查询标题
    @GetMapping("/title/detail")
    public List<SongList> likeTitle(String title){
        log.info("访问了模糊查询标题");
        return service.likeTitle(title);
    }


    // 查询风格
    @GetMapping("/style/detail")
    public List<SongList> likeStyle(String style){
        log.info("访问了查询风格接口");
        return  service.likeStyle(style);
    }

    // 通过标题查询歌单
    @GetMapping("/likeTitle/detail")
    public List<SongList> songListOfTitle(String title){
        log.info("访问了通过标题查询歌单接口");
        return  service.songListOfTitle(title);
    }

    // 添加歌单
    @PostMapping("/add")
    public Object addSongList(SongList songList){
        log.info("访问了添加歌单接口");
        JSONObject jsonObject = new JSONObject();
        int r = service.insertSongList(songList);
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","添加成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","添加失败");
            return  jsonObject;
        }
    }

    // 删除歌单
    @GetMapping("/delete")
    public Object deleteSongList(Integer id){
        log.info("访问了删除歌单接口");
        JSONObject jsonObject = new JSONObject();
        if(id == null || "".equals(id) ){
            jsonObject.put("code",2);
            jsonObject.put("msg","错误的ID");
            return jsonObject;
        }
        if(service.deleteSongList(id) > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","删除成功");
            return  jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","删除失败");
            return  jsonObject;
        }
    }

    // 更新信息
    @PostMapping("/update")
    public Object updateSongList(SongList songList){
        log.info("访问了更新歌单信息接口");
        JSONObject jsonObject = new JSONObject();
        int r = service.updateSongListMsg(songList);
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","修改成功");
            return  jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","修改失败");
            return  jsonObject;
        }
    }

    // 更新歌单图片
    @PostMapping("/img/update")
    public Object updateavatorFile(@RequestParam("file") MultipartFile avatorFile, @RequestParam("id")int id){
        log.info("访问了更新歌单图片歌单接口");
        JSONObject jsonObject = new JSONObject();
        // 判断上传的图片是否为空
        if (avatorFile.isEmpty()) {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "文件上传失败！");
            return jsonObject;
        }
        // 文件名为 UUID随机数 + 图片名称
        String fileName = UUID.randomUUID().toString()+avatorFile.getOriginalFilename();
        // 文件路径为      当前项目根目录(D:\IDEA_Project\music) +           下划线 ”  \ “  + 后端项目文件夹名 \ + 图片根目录 \+ 歌单根目录\
        String filePath = System.getProperty("user.dir") + System.getProperty("file.separator") +"server\\" + "img" + System.getProperty("file.separator") + "avatorFile" ;
        File file1 = new File(filePath);
        // 如果文件夹不存在，创建一个文件夹
        if (!file1.exists()){
            file1.mkdir();
        }

        File newAvatorFile = new File(filePath + System.getProperty("file.separator") + fileName);
        // 存储在数据库中的相对路径
        String storeAvatorPath = "/img/avatorFile/"+fileName;
        try {
            // 将上传的文件写入到新文件
            avatorFile.transferTo(newAvatorFile);
            SongList songList = new SongList();
            songList.setId(id);
            songList.setPic(storeAvatorPath);
            int r  = service.updateSongListImg(songList);
            if (r > 0){
                jsonObject.put("code", 1);
                // 发送图片相对路径给前端
                jsonObject.put("avator", storeAvatorPath);
                jsonObject.put("msg", "上传成功");
                return jsonObject;
            }else {
                jsonObject.put("code", 0);
                jsonObject.put("msg", "上传失败");
                return jsonObject;
            }
        }catch (IOException e){
            jsonObject.put("code", 0);
            jsonObject.put("msg", "上传失败" + e.getMessage());
            return jsonObject;
        }finally {
            return jsonObject;
        }
    }


}
