package com.example.server.web;

import com.alibaba.fastjson.JSONObject;
import com.example.server.entity.Admin;
import com.example.server.service.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;




@RestController
// @CrossOrigin(allowCredentials ="true" ,origins = "")
public class AdminController {
    @Resource
    private AdminService adminService;

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    // 登录
    @PostMapping("/admin/login/status")
    public Object loginAdmin(Admin admin, HttpSession session)  {
        //向前端发送键值对类型的json，PrintWriter out = response.getWriter()只能传单一json，无法满足需求
        log.info("访问了登录接口");
        JSONObject jsonObject = new JSONObject();
        String name = admin.getName();
        String password = admin.getPassword();
        int r = adminService.verifyPassword(name,password);
        if(r > 0 ){
            jsonObject.put("code",1);
            jsonObject.put("msg","登录成功");
            session.setAttribute("name",name);
            return jsonObject;
        }else{
            jsonObject.put("code", 0);
            jsonObject.put("msg", "用户名或密码错误");
            return jsonObject;
        }
    }

    // 获取管理员
    @GetMapping("/admin/getAdmin")
    public Admin getAdmin(int id){
        log.info("访问了获取管理员接口");
        return adminService.selectByPrimaryKey(id);
    }

    // 添加管理员
    @GetMapping("/admin/addAdmin")
    public Object addAdmin(Admin admin){
        JSONObject jsonObject = new JSONObject();
        int r = adminService.insert(admin);
        if(r > 0){
            jsonObject.put("msg", "成功");
        }else{
            jsonObject.put("msg", "失败");
        }
        return jsonObject;
    }

    // 更新管理员信息
    @GetMapping("/admin/updateAdmin")
    public Object updateAdmin(Admin admin){
        JSONObject jsonObject = new JSONObject();
        int r = adminService.updateByPrimaryKey(admin);
        if(r > 0){
            jsonObject.put("msg", "成功");
        }else{
            jsonObject.put("msg", "失败");
        }
        return jsonObject;
    }

    // 删除管理员
    @GetMapping("/admin/deleteAdmin")
    public Object deleteAdmin(Admin admin) {
        JSONObject jsonObject = new JSONObject();
        int r = adminService.deleteByPrimaryKey(admin.getId());
        if(r > 0){
            jsonObject.put("msg", "成功");
        }else{
            jsonObject.put("msg", "失败");
        }
        return jsonObject;
    }


}
