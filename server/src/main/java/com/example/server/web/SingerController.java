package com.example.server.web;

import com.alibaba.fastjson.JSONObject;
import com.example.server.constant.Constants;
import com.example.server.entity.Singer;
import com.example.server.service.SingerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/singer")
public class SingerController {
    @Resource
    private SingerServer singerServer;
    private static final Logger log = LoggerFactory.getLogger(AdminController.class);


    @Configuration
    public class MyPicConfig implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            String os = System.getProperty("os.name");
            if (os.toLowerCase().startsWith("win")) { // windos系统
                registry.addResourceHandler("/img/singerPic/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_WIN_PATH + "\\img\\singerPic\\");
            } else { // MAC、Linux系统
                registry.addResourceHandler("/img/singerPic/**")
                        .addResourceLocations("file:" + Constants.RESOURCE_MAC_PATH + "/img/singerPic/");
            }
        }
    }

    //查询所有歌手
    @GetMapping("")
    public List<Singer> allSinger(){

        return singerServer.allSinger();
    }

    //添加歌手
    @ResponseBody
    @GetMapping("/add")
    public Object addSinger(HttpServletRequest req){
        log.info("访问了添加歌手接口");
        JSONObject jsonObject = new JSONObject();
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String pic = req.getParameter("pic");
        String birth = req.getParameter("birth");
        String location = req.getParameter("location");
        String introduction = req.getParameter("introduction");

        Singer singer = new Singer();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myBirth = new Date();
        try {
            myBirth = dateFormat.parse(birth);
        }catch (Exception e){
            e.printStackTrace();
        }
        singer.setName(name);
        singer.setSex(new Byte(sex));
        singer.setPic(pic);
        singer.setBirth(myBirth);
        singer.setLocation(location);
        singer.setIntroduction(introduction);

        boolean res = singerServer.addSinger(singer);
        if (res){
            jsonObject.put("code", 1);
            jsonObject.put("msg", "添加成功");
            return jsonObject;
        }else {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "添加失败");
            return jsonObject;
        }
    }

    //根据歌手名查找歌手
    @GetMapping("/name/detail")
    public Object singerOfName(HttpServletRequest req){
        log.info("访问了歌手名查找歌手接口");
        String name = req.getParameter("name").trim();
        return singerServer.singerOfName(name);
    }

    //根据歌手性别查找歌手
    @GetMapping("/sex/detail")
    public Object singerOfSex(HttpServletRequest req){
        log.info("访问了根据歌手性别查找歌手接口");
        String sex = req.getParameter("sex").trim();
        return singerServer.singerOfSex(Integer.parseInt(sex));
    }

    //删除歌手
    @GetMapping("/delete")
    public Object deleteSinger(HttpServletRequest req){
        log.info("访问了删除歌手接口");
        String id = req.getParameter("id");
        return singerServer.deleteSinger(Integer.parseInt(id));
    }

    //更新歌手头像
    @ResponseBody
    @PostMapping("/avatar/update")
    public Object updateSingerPic(@RequestParam("file") MultipartFile avatorFile, @RequestParam("id")int id){
        log.info("访问了更新歌手接口");
        JSONObject jsonObject = new JSONObject();
        if (avatorFile.isEmpty()) {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "文件上传失败！");
            return jsonObject;
        }
        String fileName = System.currentTimeMillis()+avatorFile.getOriginalFilename();
        String filePath = System.getProperty("user.dir") + System.getProperty("file.separator")+"server\\"  + "img" + System.getProperty("file.separator") + "singerPic" ;
        File file1 = new File(filePath);
        if (!file1.exists()){
            file1.mkdir();
        }

        File dest = new File(filePath + System.getProperty("file.separator") + fileName);
        String storeAvatorPath = "/img/singerPic/"+fileName;
        try {
            avatorFile.transferTo(dest);
            Singer singer = new Singer();
            singer.setId(id);
            singer.setPic(storeAvatorPath);
            boolean res = singerServer.updateSingerPic(singer);
            if (res){
                jsonObject.put("code", 1);
                jsonObject.put("pic", storeAvatorPath);
                jsonObject.put("msg", "上传成功");
                return jsonObject;
            }else {
                jsonObject.put("code", 0);
                jsonObject.put("msg", "上传失败");
                return jsonObject;
            }
        }catch (IOException e){
            jsonObject.put("code", 0);
            jsonObject.put("msg", "上传失败" + e.getMessage());
            return jsonObject;
        }finally {
            return jsonObject;
        }
    }

    //更新歌手信息
    @ResponseBody
    @GetMapping("/update")
    public Object updateSingerMsg(HttpServletRequest req){
        log.info("访问了更新歌手信息接口");
        JSONObject jsonObject = new JSONObject();
        String id = req.getParameter("id").trim();
        String name = req.getParameter("name").trim();
        String sex = req.getParameter("sex").trim();
        String pic = req.getParameter("pic").trim();
        String birth = req.getParameter("birth").trim();
        String location = req.getParameter("location").trim();
        String introduction = req.getParameter("introduction").trim();

        Singer singer = new Singer();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myBirth = new Date();
        try {
            myBirth = dateFormat.parse(birth);
        }catch (Exception e){
            e.printStackTrace();
        }
        singer.setId(Integer.parseInt(id));
        singer.setName(name);
        singer.setSex(new Byte(sex));
        singer.setPic(pic);
        singer.setBirth(myBirth);
        singer.setLocation(location);
        singer.setIntroduction(introduction);

        boolean res = singerServer.updateSingerMsg(singer);
        if (res){
            jsonObject.put("code", 1);
            jsonObject.put("msg", "修改成功");
            return jsonObject;
        }else {
            jsonObject.put("code", 0);
            jsonObject.put("msg", "修改失败");
            return jsonObject;
        }
    }
}
