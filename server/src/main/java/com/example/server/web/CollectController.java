package com.example.server.web;

import com.alibaba.fastjson.JSONObject;

import com.example.server.entity.Collect;
import com.example.server.service.CollectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/collection")
public class CollectController {
    @Resource
    private CollectService collectService;
    private static final Logger log = LoggerFactory.getLogger(CollectService.class);

    //所有用户收藏
    @GetMapping("")
    public List<Collect> allCollect(){
        log.info("访问了所有用户收藏");
        return  collectService.allCollect();
    }

    // 添加到收藏夹
    @PostMapping("/add")
    public Object addCollect(Collect collect){
        log.info("访问了添加用户收藏接口");
        JSONObject jsonObject = new JSONObject();
        Integer userId = collect.getUserId();
        Integer songId = collect.getSongId();
        byte type = collect.getType();
        if( songId == null || "".equals(songId)){
            jsonObject.put("code",0);
            jsonObject.put("msg","收藏歌曲为空");
            return jsonObject;
        }else if(collectService.existSongId(userId,songId) > 0){
            jsonObject.put("code",2);
            jsonObject.put("msg","已收藏");
            return  jsonObject;
        }
        collect.setCreateTime(new Date());
        int r = collectService.insert(collect);
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","收藏成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","收藏失败");
            return  jsonObject;
        }

    }

    // 用户收藏夹
    @GetMapping("/detail")
    public List<Collect> userCollect(Integer userId){
        log.info("访问了用户收藏夹接口");
        return collectService.collectionOfUser(userId);
    }

    // 删除收藏的歌曲
    @GetMapping("/delete")
    public Object deleteCollect(Collect collect){
        log.info("访问了删除收藏的歌曲接口");
        JSONObject jsonObject = new JSONObject();
        int r = collectService.deleteCollect(collect.getUserId(),collect.getSongId());
        if(r > 0){
            jsonObject.put("code",1);
            jsonObject.put("msg","删除成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","删除失败");
            return jsonObject;
        }
    }

    //更新收藏夹
    @PostMapping("/update")
    public Object updateCollect(Collect collect){
        log.info("访问了更新收藏夹方法接口");
        JSONObject jsonObject = new JSONObject();
        int r = collectService.updateCollectMsg(collect);
        if (r > 0 ){
            jsonObject.put("code",1);
            jsonObject.put("msg","修改成功");
            return jsonObject;
        }else{
            jsonObject.put("code",0);
            jsonObject.put("msg","修改失败");
            return jsonObject;
        }

    }


}
