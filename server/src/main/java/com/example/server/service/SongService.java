package com.example.server.service;

import com.example.server.entity.Song;
import java.util.List;

public interface SongService {
    int deleteByPrimaryKey(Integer id);

    int insert(Song song);

    int insertSelective(Song song);

    Song selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Song song);

    int updateByPrimaryKeyWithBLOBs(Song song);

    int updateByPrimaryKey(Song song);

    boolean updateSongMsg(Song song);

    boolean updateSongUrl(Song song);

    boolean updateSongPic(Song song);

    int deleteSong(Integer id);

    List<Song> allSong();

    List<Song> songOfSingerId(Integer singerId);

    List<Song> songOfId(Integer id);

    List<Song> songOfSingerName(String name);

    List<Song> songOfName(String name);

    boolean addSong(Song song);
}
