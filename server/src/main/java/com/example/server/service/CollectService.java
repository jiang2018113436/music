package com.example.server.service;

import com.example.server.entity.Collect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectService {
    int deleteByPrimaryKey(Integer id);
    //插入
    int insert(Collect collect);
    //选择性插入数据
    int insertSelective(Collect collect);
    //查询
    Collect selectByPrimaryKey(Integer id);
    //根据ID更新
    int updateByPrimaryKey(Collect collect);
    //判断歌
    int existSongId( Integer userId, Integer songId);
    //更新信息
    int updateCollectMsg(Collect collect);
    //删除收藏歌单
    int deleteCollect(Integer userId, Integer songId);
    //查询全部歌单
    List<Collect> allCollect();
    //用户歌单
    List<Collect> collectionOfUser(Integer userId);
}
