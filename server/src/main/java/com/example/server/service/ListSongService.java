package com.example.server.service;

import com.example.server.entity.ListSong;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ListSongService {
    int insertSelective(ListSong listSong);

    ListSong selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(ListSong listSong);

    int updateListSongMsg(ListSong listSong);

    int existListSong( Integer songId, Integer songListId);

    int deleteListSongOfSong(Integer songId, Integer songListId);

    int deleteListSong(Integer songId);
    // 查询所有
    List<ListSong> allListSong();
    // 查询歌单所含的所有音乐
    List<ListSong> listSongOfSongId(Integer songListId);
}
