package com.example.server.service.impl;

import com.example.server.dao.SingerDao;
import com.example.server.entity.Singer;
import com.example.server.service.SingerServer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SingerServerImpl implements SingerServer {

    @Resource
    private SingerDao singerDao;

    @Override
    public boolean updateSingerMsg(Singer singer) {
        return singerDao.updateSingerMsg(singer) >0 ?true:false;
    }

    @Override
    public boolean updateSingerPic(Singer singer) {

        return singerDao.updateSingerPic(singer) >0 ?true:false;
    }

    @Override
    public boolean deleteSinger(Integer id) {
        return singerDao.deleteSinger(id) >0 ?true:false;
    }

    @Override
    public List<Singer> allSinger()
    {
        return singerDao.allSinger();
    }

    @Override
    public int insert(Singer singer) {
        return 0;
    }

    @Override
    public boolean addSinger(Singer singer)  {

        return singerDao.insertSelective(singer) > 0 ? true : false;
    }

    @Override
    public List<Singer> singerOfName(String name)

    {
        return singerDao.singerOfName(name);
    }

    @Override
    public List<Singer> singerOfSex(Integer sex)

    {
        return singerDao.singerOfSex(sex);
    }
}
