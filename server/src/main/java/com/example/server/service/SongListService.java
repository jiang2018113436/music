package com.example.server.service;

import com.example.server.entity.SongList;

import java.util.List;

public interface SongListService {
    // 选择性插入
    int insertSongList(SongList songList);
    // 全插入
    int insertSelective(SongList songList);
    // 查询
    SongList selectByPrimaryKey(Integer id);
    // 全修改
    int updateByPrimaryKey(SongList songList);
    // 选择性修改
    int updateSongListMsg(SongList songList);
    // 修改图片
    int updateSongListImg(SongList songList);
    // 删除
    int deleteSongList(Integer id);
    // 全部歌单信息
    List<SongList> allSongList();
    // 模糊查询标题
    List<SongList> likeTitle(String title);
    // 模糊查询音乐风格
    List<SongList> likeStyle(String style);
    // 通过标题查询
    List<SongList> songListOfTitle(String title);
}
