package com.example.server.service;

import com.example.server.entity.Singer;

import java.util.List;

public interface SingerServer {

    int insert(Singer singer);
    boolean addSinger (Singer singer);

    boolean updateSingerMsg(Singer singer);

    boolean updateSingerPic(Singer singer);

    boolean deleteSinger(Integer id);

    List<Singer> allSinger();

    List<Singer> singerOfName(String name);

    List<Singer> singerOfSex(Integer sex);
}
