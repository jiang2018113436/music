package com.example.server.service.impl;

import com.example.server.dao.ConsumerDao;
import com.example.server.entity.Consumer;
import com.example.server.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    @Autowired
    private ConsumerDao consumerDao;

    @Override
    public int addUser(Consumer consumer) {
        return consumerDao.insertSelective(consumer);
    }

    @Override
    public int updateUserMsg(Consumer consumer) {
        return consumerDao.updateUserMsg(consumer);
    }

    @Override
    public int updateUserAvator(Consumer consumer) {return consumerDao.updateUserAvator(consumer);}

    @Override
    public int existUser(String username) {
        return consumerDao.existUsername(username);
    }

    @Override
    public int veritypasswd(String username, String password)
    {return consumerDao.verifyPassword(username, password);}

//    删除用户
    @Override
    public int deleteUser(Integer id) {
        return consumerDao.deleteUser(id);
    }

    @Override
    public List<Consumer> allUser() {
        return consumerDao.allUser();
    }

    @Override
    public List<Consumer> userOfId(Integer id) {return consumerDao.userOfId(id);}

    @Override
    public List<Consumer> loginStatus(String username) {return consumerDao.loginStatus(username);}
}
