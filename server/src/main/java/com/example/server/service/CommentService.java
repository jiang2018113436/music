package com.example.server.service;


import com.example.server.entity.Comment;

import java.util.List;

public interface CommentService {

    //添加评论
    boolean addComment(Comment comment);
    //修改评论信息
    boolean updateCommentMsg(Comment comment);
    //删除评论
    boolean deleteComment(Integer id);
    //获取所有评论
    List<Comment> allComment();
    //获取指定歌曲的所有评论
    List<Comment> commentOfSongId(Integer songId);
    //获取指定歌单的所有评论
    List<Comment> commentOfSongListId(Integer songListId);

}
