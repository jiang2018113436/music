package com.example.server.service;

import com.example.server.entity.Rank;

public interface RankService {

    int rankOfSongListId(Long songListId);

    boolean  addRank(Rank rank);
}
