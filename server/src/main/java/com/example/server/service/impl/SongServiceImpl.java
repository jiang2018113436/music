package com.example.server.service.impl;

import com.example.server.dao.SongDao;
import com.example.server.entity.Song;
import com.example.server.service.SongService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SongServiceImpl implements SongService {
    @Resource
    private SongDao songDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return songDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Song song) {
        return songDao.insert(song);
    }

    @Override
    public int insertSelective(Song song) {
        return songDao.insertSelective(song);
    }

    @Override
    public Song selectByPrimaryKey(Integer id) {
        return songDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Song song) {
        return songDao.updateByPrimaryKeySelective(song);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(Song song) {
        return songDao.updateByPrimaryKeyWithBLOBs(song);
    }

    @Override
    public int updateByPrimaryKey(Song song) {
        return songDao.updateByPrimaryKey(song);
    }

    @Override
    public boolean updateSongMsg(Song song) {
        return songDao.insertSelective(song) > 0?true:false;
    }

    @Override
    public boolean updateSongUrl(Song song) {
        return songDao.updateSongMsg(song) > 0?true:false;
    }

    @Override
    public boolean updateSongPic(Song song) {
        return songDao.updateSongMsg(song) > 0?true:false;
    }

    @Override
    public int deleteSong(Integer id) {
        return songDao.deleteSong(id);
    }

    @Override
    public List<Song> allSong() {
        return songDao.allSong();
    }

    @Override
    public List<Song> songOfSingerId(Integer singerId) {
        return songDao.songOfSingerId(singerId);
    }

    @Override
    public List<Song> songOfId(Integer id) {
        return songDao.songOfId(id);
    }

    @Override
    public List<Song> songOfSingerName(String name) {
        return songDao.songOfSingerName(name);
    }

    @Override
    public List<Song> songOfName(String name) {
        return songDao.songOfName(name);
    }

    @Override
    public boolean addSong(Song song) {
        return songDao.addSong(song) ?true:false;
    }
}
