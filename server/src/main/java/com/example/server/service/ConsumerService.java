package com.example.server.service;



import com.example.server.entity.Consumer;

import java.util.List;

public interface ConsumerService {

    int addUser(Consumer consumer);

    int updateUserMsg(Consumer consumer);

    int updateUserAvator(Consumer consumer);

    int existUser(String username);

    int veritypasswd(String username, String password);

    int deleteUser(Integer id);

    List<Consumer> allUser();

    List<Consumer> userOfId(Integer id);

    List<Consumer> loginStatus(String username);

}
