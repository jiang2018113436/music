package com.example.server.service.impl;

import com.example.server.dao.SongListDao;
import com.example.server.entity.SongList;
import com.example.server.service.SongListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SongListServiceImpl implements SongListService {
    @Resource
    private SongListDao songListDao;
    @Override
    public int insertSongList(SongList songList) {
        return songListDao.insertSongList(songList);
    }

    @Override
    public int insertSelective(SongList songList) {
        return songListDao.insertSelective(songList);
    }

    @Override
    public SongList selectByPrimaryKey(Integer id) {
        return songListDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(SongList songList) {
        return songListDao.updateByPrimaryKey(songList);
    }

    @Override
    public int updateSongListMsg(SongList songList) {
        return songListDao.updateSongListMsg(songList);
    }

    @Override
    public int updateSongListImg(SongList songList) {
        return songListDao.updateSongListImg(songList);
    }

    @Override
    public int deleteSongList(Integer id) {
        return songListDao.deleteSongList(id);
    }

    @Override
    public List<SongList> allSongList() {
        return songListDao.allSongList();
    }

    @Override
    public List<SongList> likeTitle(String title) {
        return songListDao.likeTitle(title);
    }

    @Override
    public List<SongList> likeStyle(String style) {
        return songListDao.likeStyle(style);
    }

    @Override
    public List<SongList> songListOfTitle(String title) {
        return songListDao.songListOfTitle(title);
    }
}
