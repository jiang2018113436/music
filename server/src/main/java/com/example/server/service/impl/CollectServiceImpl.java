package com.example.server.service.impl;

import com.example.server.dao.CollectDao;
import com.example.server.entity.Collect;
import com.example.server.service.CollectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CollectServiceImpl implements CollectService {
    @Resource
    private CollectDao collectDao;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return collectDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Collect collect) {
        return collectDao.insert(collect);
    }

    @Override
    public int insertSelective(Collect collect) {
        return collectDao.insertSelective(collect);
    }

    @Override
    public Collect selectByPrimaryKey(Integer id) {
        return collectDao.selectByPrimaryKey(id);
    }



    @Override
    public int updateByPrimaryKey(Collect collect) {
        return collectDao.updateByPrimaryKey(collect);
    }

    @Override
    public int existSongId(Integer userId, Integer songId) {
        return collectDao.existSongId(userId,songId);
    }

    @Override
    public int updateCollectMsg(Collect collect) {
        return collectDao.updateCollectMsg(collect);
    }

    @Override
    public int deleteCollect(Integer userId, Integer songId) {
        return collectDao.deleteCollect(userId,songId);
    }

    @Override
    public List<Collect> allCollect() {
        return collectDao.allCollect();
    }

    @Override
    public List<Collect> collectionOfUser(Integer userId) {
        return collectDao.collectionOfUser(userId);
    }
}
