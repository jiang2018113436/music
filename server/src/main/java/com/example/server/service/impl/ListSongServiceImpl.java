package com.example.server.service.impl;

import com.example.server.dao.ListSongDao;
import com.example.server.entity.ListSong;
import com.example.server.service.ListSongService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ListSongServiceImpl implements ListSongService {
    @Resource
    private ListSongDao listSongDao;
    @Override
    public int insertSelective(ListSong listSong) {
        return listSongDao.insertSelective(listSong);
    }

    @Override
    public ListSong selectByPrimaryKey(Integer id) {
        return listSongDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(ListSong listSong) {
        return listSongDao.updateByPrimaryKey(listSong);
    }

    @Override
    public int updateListSongMsg(ListSong listSong) {
        return listSongDao.updateListSongMsg(listSong);
    }

    @Override
    public int existListSong(Integer songId, Integer songListId) {
        return listSongDao.existListSong(songId,songListId);
    }

    @Override
    public int deleteListSongOfSong(Integer songId, Integer songListId) {
        return listSongDao.deleteListSongOfSong(songId,songListId);
    }

    @Override
    public int deleteListSong(Integer songListId) {
        return listSongDao.deleteListSong(songListId);
    }

    @Override
    public List<ListSong> allListSong() {
        return listSongDao.allListSong();
    }

    @Override
    public List<ListSong> listSongOfSongId(Integer songListId) {
        return listSongDao.listSongOfSongId(songListId);
    }
}
