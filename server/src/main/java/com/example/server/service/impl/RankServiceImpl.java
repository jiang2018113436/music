package com.example.server.service.impl;

import com.example.server.dao.RankDao;
import com.example.server.entity.Rank;
import com.example.server.service.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RankServiceImpl implements RankService {
    @Autowired
    private RankDao rankDao;

    @Override
    public int rankOfSongListId(Long songListId) {
        return rankDao.selectScoreSum(songListId) / rankDao.selectRankNum(songListId);
    }

    @Override
    public boolean addRank(Rank rank) {

        return rankDao.insert(rank) > 0 ? true:false;
    }
}
