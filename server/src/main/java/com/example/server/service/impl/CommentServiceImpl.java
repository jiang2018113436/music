package com.example.server.service.impl;

import com.example.server.dao.CommentDao;
import com.example.server.entity.Comment;
import com.example.server.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;

    @Override
    public boolean addComment(Comment comment) {
        return commentDao.insertSelective(comment) > 0 ? true : false;
    }

    @Override
    public boolean updateCommentMsg(Comment comment) {
        return commentDao.updateCommentMsg(comment) > 0 ? true : false;
    }

    @Override
    public boolean deleteComment(Integer id) {
        return commentDao.deleteComment(id) > 0 ? true : false;
    }

    @Override
    public List<Comment> allComment() {
        return commentDao.allComment();
    }

    @Override
    public List<Comment> commentOfSongId(Integer songId) {
        return commentDao.commentOfSongId(songId);
    }

    @Override
    public List<Comment> commentOfSongListId(Integer songListId) {
        return commentDao.commentOfSongListId(songListId);
    }
}
