package com.example.server.service.impl;

import com.example.server.dao.AdminDao;
import com.example.server.entity.Admin;
import com.example.server.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminDao adminDao;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return adminDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Admin admin) {
        return adminDao.insert(admin);
    }

    @Override
    public int insertSelective(Admin admin) {
        return adminDao.insertSelective(admin);
    }

    @Override
    public Admin selectByPrimaryKey(Integer id) {
        return adminDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Admin admin) {
        return adminDao.updateByPrimaryKeySelective(admin);
    }

    @Override
    public int updateByPrimaryKey(Admin admin) {
        return adminDao.updateByPrimaryKey(admin);
    }

    @Override
    public int verifyPassword(String username, String password) {
        return adminDao.verifyPassword(username,password);
    }
}
